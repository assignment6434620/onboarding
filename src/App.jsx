import Header from "./Header"
import Main from "./Main-section"
import AddCompetitor from "./competitorAppSection/AddCompetitor";
import YourApp from "./yourAppCompo/YourApp"
import 'bootstrap/dist/css/bootstrap.css';

function App() {


  return (

    <>
      <Header />
      <div style={{ margin: '1rem 2rem'}}>
        <Main />
        <YourApp/>
        <AddCompetitor/>
      </div>

    </>
  )
}

export default App
