import Navbar from 'react-bootstrap/Navbar';

function Header() {
  return (
    <>
      <Navbar style={{backgroundColor:'white'}}>
     
          <Navbar.Brand href="#home">
            <img 
              src="/logo.svg"
              width="125"
              height="25"
              className="d-inline-block align-top mx-3"
              alt="React Bootstrap logo"
            />
          </Navbar.Brand>
       
      </Navbar>
    
      
    </>
  );
}

export default Header;