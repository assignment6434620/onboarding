import React from "react";
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { Container } from "react-bootstrap";
import Number from "./Number";
import { isAddAppsCompleteAtom } from "./yourAppCompo/YourApp";
import { useAtom } from "jotai";


function Main() {
    const [isAddAppsComplete]=useAtom(isAddAppsCompleteAtom);

    return (
        <section id="main-section" style={{borderRadius:"12.5px"}}>
            <Container>
                <Row>
                    <Col md="4">
                        <Number color={isAddAppsComplete?"white":"#4fc9da"} bgColor={isAddAppsComplete?"#4fc9da":"#ddf8fc"}  number={isAddAppsComplete?"✓":"1"} />
                        <p className="main-info">Add your App</p>
                    </Col>
                    <Col md="4">
                        <Number bgColor="#F8F6F2" color="black"  number="2" />
                        <p className="main-info">Add competitor App</p>
                    </Col>
                    <Col md="4">
                    <Number bgColor="#F8F6F2" color="black"  number="3" />
                        <p className="main-info">Choose keywords</p>
                    </Col>
                </Row>
            </Container>



        </section>
    )
}

export default Main;