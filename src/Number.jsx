import React from "react";

function Number(props){
    return(
        <div className="h-35px w-35px" style={{width:'fit-content',marginRight:'auto',marginLeft:'auto',backgroundColor:`${props.bgColor}`,borderRadius:"10px",padding:".25rem 0 0 0 "}}>
            <h3 style={{textAlign:"center",color:`${props.color}`,fontSize:"1.75rem !important"}}>{props.number}</h3>
            
        </div>
    )
}

export default Number;
