import React, { useState } from "react";
import {  Col, Container, Row } from "react-bootstrap";

import { isAddAppsCompleteAtom } from "../yourAppCompo/YourApp";
import { useAtom } from "jotai";



function AddCompetitor() {

    const [isAddAppsComplete,setIsAddAppsComplete]=useAtom(isAddAppsCompleteAtom);

    function handleBackBtn(){
        setIsAddAppsComplete(false);
    }

    return (
        <section style={{ height: '62vh',display:isAddAppsComplete?'block':'none' }} id="your-app">
            <Container>
                <Row>
                    <Col lg="4">
                     
                    </Col>
                    <Col lg="3">
                      
                    </Col>
                    <Col lg="5">
                        
                    </Col>
                </Row>
                
                <Row  style={{position:"relative",marginTop:"12.5%"}} >
                    <Col lg="12" className="text-center">
                        <button onClick={handleBackBtn} href="#" className="btn btn-primary">back</button>

                       
                    </Col>
                </Row>

            </Container>
          
        </section>
    );
}

export default AddCompetitor;
