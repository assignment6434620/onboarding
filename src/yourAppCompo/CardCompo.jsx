import { useEffect, useState, forwardRef } from 'react';
import { Row, Col } from 'react-bootstrap';

const CardCompo = forwardRef((props, ref) => {
  const [isMouseOver, setIsMouseOver] = useState(false);
  const { isActive, onMouseEnter, onMouseLeave } = props;
  
  const handleMouseOver = () => {
    setIsMouseOver(true);
    if (onMouseEnter) {
      onMouseEnter();
    }
  };

  const handleMouseLeave = () => {
    setIsMouseOver(false);
    if (onMouseLeave) {
      onMouseLeave();
    }
  };
  const handleClick = () => {
    props.selectedApp(props);
  };

  return (
    <Row
      className={`${isMouseOver || isActive ? 'overlay' : ''}`}
      onMouseOver={handleMouseOver}
      onMouseLeave={handleMouseLeave}
      onClick={handleClick}
      ref={ref}
    >
      <Col lg="4" className="d-flex align-items-center justify-content-center">
        <img
          src={props.app_icon}
          alt={props.title}
          style={{
            display: 'block',
            width: '48px',
            height: '48px',
            borderRadius: '10px',
          }}
        />
      </Col>
      <Col className="pt-3" lg="8">
        <h6>{props.title}</h6>
        <Row>
          <Col className="d-flex align-items-center justify-content-center" lg="7">
            <p style={{ fontSize: '.75rem', display: 'block' }}>{props.developer_name}</p>
          </Col>
          <Col lg="5">
            <span>⭐ {props.rating}</span>
          </Col>
        </Row>
      </Col>
    </Row>
  );
});

export default CardCompo;
