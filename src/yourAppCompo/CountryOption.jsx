import { atom, useAtom } from "jotai";
import React, { useState } from "react";

export const createIsCountrySelectedAtom = () => {
  return atom(false);
};

function CountryOption(props) {
  const isCountrySelectedAtom = React.useMemo(createIsCountrySelectedAtom, []);

  const [isMouseOver, setIsMouseOver] = useState(false);
  const [isCountrySelected, setCountrySelected] = useAtom(isCountrySelectedAtom);
  function handleClick() {
    setCountrySelected(true);
    props.selectedCountry(props);
    
  }

  const handleMouseOver = () => {
    setIsMouseOver(true);
  };

  const handleMouseLeave = () => {
    setIsMouseOver(false);
  };

  return (
    <>
      <div
        className={`${isMouseOver ? "overlay d-flex p-2" : "d-flex p-2"}`}
        onClick={handleClick}
        onMouseOver={handleMouseOver}
        onMouseLeave={handleMouseLeave}
      >
        <div>
          <img width={32} height={24} src={props.imageUrl} alt={props.country_name} />
        </div>
        <div className="mx-3">
          <p>{props.country_name}</p>
        </div>
      </div>
    </>
  );
}

export default CountryOption;
