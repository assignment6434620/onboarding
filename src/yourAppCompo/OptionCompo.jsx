import React from "react";
import { atom,useAtom } from "jotai";

const isMouseOver=atom(false);
const isMouseLeave=atom(false);


function OptionCompo(props) {
    const [isMouseOver, setMouseOver] = useAtom(isMouseOver);
    const [isMouseLeave, setMouseLeave] = useAtom(isMouseLeave);


    function handleMouseOver() {
        setMouseOver(true);
        setMouseLeave(false);
    }

    function handleMouseLeave() {
        setMouseLeave(true);
        setMouseOver(false);
    }
    return (
        <div  className={(isMouseOver && !isMouseLeave) ? 'overlay' : ''}
            onMouseOver={handleMouseOver} onMouseLeave={handleMouseLeave}
        >
            <option className='mt-2 mx-1' style={{ fontSize: '1.2rem' }} >{props.country}</option>

        </div>
    )
}export default OptionCompo;