import React from "react";
import { Col, Container, Row } from "react-bootstrap";
import { Tooltip } from 'react-tooltip'
import 'react-toastify/dist/ReactToastify.css';


function Platform() {


    return (
        <Container>
            <h3>Select Platform</h3>
            <Row style={{backgroundColor:"#FDFCFB"}} className="mx-2">

                <Col>

                    <div className="d-flex" style={{justifyContent:"space-between"}}>
                        <div className="form-check form-check-custom form-check-solid my-3">
                            <input className="form-check-input" type="radio" value="" id="flexRadioDefault" defaultChecked />
                            <label style={{ fontSize: "1.25rem" }} className="form-check-label" htmlFor="flexRadioDefault">
                                Android  </label>
                        </div>
                        <div className="my-3">
                            <i className="fa-brands fa-google-play fa-2xl " style={{ color: "black" }}></i>
                        </div>

                    </div>
                    <div className="d-flex IOS"  style={{justifyContent:"space-between"}}>
                        <div className="form-check form-check-custom form-check-solid my-3 ">
                            <input data-tip=" feature coming soon" className="form-check-input" type="radio" value="" disabled id="flexRadioDisabled" />
                            <label style={{ fontSize: "1.25rem" }}
                                className="form-check-label"
                                htmlFor="flexRadioDisabled"

                            >
                                IOS
                            </label>

                        </div>
                        <div className="my-4    ">
                            <i className="fa-brands fa-app-store fa-2xl "></i>

                        </div>

                    </div>

                    <Tooltip anchorSelect=".IOS" place="left">
                        IOS support coming soon!!
                    </Tooltip>


                </Col>
            
            </Row>
        </Container>

    )
}

export default Platform;