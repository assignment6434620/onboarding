// Search.js
import React from 'react';
import { QueryClientProvider } from '@tanstack/react-query';
import SearchApp from './SearchApp';
import queryClient from './QueryClient';

export default function Search() {
  return (
    <QueryClientProvider client={queryClient}>
      <SearchApp />
    </QueryClientProvider>
  );
}
