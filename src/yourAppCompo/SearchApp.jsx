import { atom, useAtom } from 'jotai';
import { useQuery } from '@tanstack/react-query';
import { Container, Row, Col } from 'react-bootstrap';
import CardCompo from './CardCompo';
import { v4 as uuidv4 } from 'uuid';
import queryClient from './QueryClient';
import React, { useEffect, useRef, useState } from 'react';

const searchDataAtom = atom('');
const selectedAppAtom = atom([]);
const createIsAppSelectedAtom = () => {
    return atom(false);
};

export const isAppSelectedAtom = createIsAppSelectedAtom();

const fetchData = (searchData) =>
    fetch(`https://jsonserver.maakeetoo.com/search-app?title_like=${searchData}`).then((res) => res.json());

function SearchApp() {
    const [activeIndex, setActiveIndex] = useState(0);
    const [searchData, setSearchData] = useAtom(searchDataAtom);
    const [selectedApp, setSelectedApp] = useAtom(selectedAppAtom);
    const [isAppSelected, setAppSelected] = useAtom(isAppSelectedAtom);
    const [debouncedSearchData, setDebouncedSearchData] = useState('');
    const dropdownRef = useRef(null);
    const cardCompoRefs = useRef([]);
    console.log("active" + activeIndex);

    const { isLoading, error, data } = useQuery({
        queryKey: [debouncedSearchData],
        queryFn: () => fetchData(debouncedSearchData),
        enabled: debouncedSearchData !== '',
        staleTime: 300000,
        client: queryClient,
    });

    useEffect(() => {
        const handleKeyDown = (event) => {
            console.log(event.key);
            if (event.key === 'ArrowDown') {
                event.preventDefault();
                setActiveIndex((prevIndex) => {
                    const newIndex = prevIndex + 1;
                    return newIndex < data.length ? newIndex : prevIndex;
                });
                if (data.length - 1 > activeIndex) {
                    cardCompoRefs.current[activeIndex + 1].scrollIntoView({ behavior: 'smooth', block: 'nearest' });

                } else {
                    setActiveIndex(0);

                }


            } else if (event.key === 'ArrowUp') {
                event.preventDefault();
                setActiveIndex((prevIndex) => {
                    const newIndex = prevIndex - 1;
                    return newIndex >= 0 ? newIndex : prevIndex;
                });
                if (activeIndex > 0) {
                    console.log(activeIndex + "active inside");
                    cardCompoRefs.current[activeIndex - 1].scrollIntoView({ behavior: 'smooth', block: 'nearest' });

                } else {
                    setActiveIndex(data.length - 1);
                }
            } else if (event.key === 'Enter') {
                event.preventDefault();
                if (data.length > 0) {
                    const currentCardProps = data[activeIndex];
                    console.log(currentCardProps);
                    setSelectedApp(currentCardProps);
                    setAppSelected(true);

                }
            }


        };

        const scrollableDiv = dropdownRef.current;





        scrollableDiv.tabIndex = 0; // Add tabindex attribute
        scrollableDiv.addEventListener('keydown', handleKeyDown);
        scrollableDiv.focus(); // Set focus on the scrollableDiv



        return () => {
            scrollableDiv.removeEventListener('keydown', handleKeyDown);

        };
    }, [data, activeIndex]);




    function handleChange(e) {
        setSearchData(e.target.value);
    }

    function handleClose() {
        setAppSelected(false);
    }

    function handleMouseEnter(index) {
        setActiveIndex(index);
    }

    function handleMouseLeave() {
        // Reset activeIndex to the currently selected app
        setActiveIndex(data.findIndex((item) => item.title === selectedApp.title));
    }

    function callBack(app) {
        setSelectedApp(app);
        setAppSelected(true);
    }

    useEffect(() => {
        const debounceTimer = setTimeout(() => {
            setDebouncedSearchData(searchData);
        }, 500); // Adjust the debounce delay as needed

        return () => {
            clearTimeout(debounceTimer);
        };
    }, [searchData]);

    return (
        <>
            <h3>Search your App</h3>

            <Container style={{ display: isAppSelected ? 'block' : 'none', border: '1px solid #4fc9da', borderRadius: '5px' }}>
                <Row >
                    <Col lg="4" className='d-flex align-items-center justify-content-center text-center'>
                        <img
                            src={selectedApp.app_icon}
                            alt={selectedApp.title}
                            style={{
                                display: 'block',
                                width: '48px',
                                height: '48px',
                                borderRadius: '10px',
                            }}
                        />
                    </Col>

                    <Col className="pt-3" lg="7">
                        <h6>{selectedApp.title}  </h6>
                        <Row>
                            <Col lg="7">
                                <p style={{ fontSize: ".75rem" }}>{selectedApp.developer_name
                                } </p>
                            </Col>
                            <Col lg="5">
                                <span>⭐ {selectedApp.rating}</span>

                            </Col>
                        </Row>

                    </Col>
                    <Col lg="1">
                        <i onClick={handleClose} className="fa-solid fa-xmark fa-lg" style={{ color: '#d92708', cursor: 'pointer' }}></i>
                    </Col>


                </Row>

            </Container>



            <Container style={{ display: isAppSelected ? 'none' : 'block' }}>
                <div>

                    <Row className="mt-3">
                        <Col lg="12">

                            <div className="w-lg-100 position-relative">
                                <input className="form-control form-control-solid" type="text" value={searchData} onChange={handleChange} placeholder="Search android apps" />

                                <div className="position-absolute translate-middle-y top-50 end-0 me-3">
                                    <span className="svg-icon svg-icon-2hx">
                                        <i className="fa-solid fa-magnifying-glass fa-xl mt-3 "></i>
                                    </span>
                                </div>
                            </div>
                        </Col>
                        <Col className="mt-2" lg="12">

                            <div className='card shadow-sm text-center p-2' style={{ display: isLoading && searchData !== '' ? 'block' : 'none' }}>
                                <h6>Loading...</h6>
                            </div>
                            <div className='card shadow-sm text-center p-2' style={{ display: !isLoading && data.length === 0 ? 'block' : 'none' }}>
                                <h6>No App Found</h6>
                            </div>

                            <div className="col-lg-12" style={{ position: 'relative' }}>
                                <div
                                    ref={dropdownRef}
                                    className="card shadow-sm scrollable"
                                    style={{
                                        position: 'absolute',
                                        left: 0,
                                        right: 0,
                                        zIndex: 2,
                                        height: '220px',
                                        overflowY: 'auto',
                                        display: !isLoading && searchData !== '' && data.length !== 0 ? 'block' : 'none',
                                        cursor: 'pointer',
                                        outline: "none",
                                        borderColor: 'transparent'
                                    }}
                                >
                                    {data &&
                                        data.map((item, index) => {
                                            const id = uuidv4();
                                            const isActive = index === activeIndex;

                                            return (
                                                <CardCompo
                                                    key={id}
                                                    app_icon={item.app_icon}
                                                    title={item.title}
                                                    rating={item.rating}
                                                    developer_name={item.developer_name}
                                                    selectedApp={callBack}
                                                    isActive={isActive}
                                                    onMouseEnter={() => handleMouseEnter(index)}
                                                    onMouseLeave={handleMouseLeave}
                                                    ref={(ref) => (cardCompoRefs.current[index] = ref)}
                                                />
                                            );
                                        })}
                                </div>
                            </div>

                        </Col>
                    </Row >
                </div >


            </Container >

        </>




    );
};

export default SearchApp;
