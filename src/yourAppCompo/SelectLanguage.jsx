import { useAtom } from 'jotai';
import { atom } from 'jotai';
import { v4 as uuidv4 } from 'uuid';
import React, { useEffect, useRef, useState } from 'react';

// Checkbox component
const Checkbox = ({ value, checked, onChange }) => (
  <div>
    <input className='dropdownInput'
      style={{ cursor: 'pointer', height: '100%' }}
      type="checkbox"
      value={value}
      onChange={onChange}
      checked={checked}
    />
  </div>
);

// LanguageName component
const LanguageName = ({ name, onClick }) => (
  <div  style={{ width: '100%' }}>
    <p 
      style={{ cursor: 'pointer', height: '100%', display: 'flex',
      alignItems: 'center',
       }}
      onClick={onClick}
      className="mx-2 dropdownInput"
     
    >
      {name}
    </p>
  </div>
);



function Dropdown({
  language,
  selectedLanguages,
  handleCheckboxChange,
  handleSelectCheck,
  handleLangClick,
  handleSelectClick,
  hoveredIndex,
  handleMouseOver,
  handleMouseOut,
  isHover,
  selectMouseOver,
  selectMouseLeave
}) {
  return (
    <div
      className="card dropdownInput"
      style={{
        position: 'absolute',
        left: 0,
        right: 0,
        zIndex: 2,
        height: '220px',
        overflowY: 'auto',
        display: 'block',
      }}
    >
      <div
        className={`d-flex p-2 ${isHover ? 'overlay' : ''}`}
        onMouseOver={selectMouseOver}
        onMouseLeave={selectMouseLeave}
      >
        <div>
          <input
            style={{ cursor: 'pointer', height: '100%' }}
            type="checkbox"
            value={'selectAll'}
            onChange={handleSelectCheck}
            checked={language.length === selectedLanguages.length}
          />
        </div>
        <div style={{ width: '100%' }}>
          <p
            style={{ cursor: 'pointer', outline: 'none', height: '100%',display: 'flex',
            alignItems: 'center', }}
            onClick={handleSelectClick}
            className="mx-2"
          >
            Select All
          </p>
        </div>
      </div>

      {language.map((item, i) => {
        const id = uuidv4();
        return (
          <div
            key={id}
            className={`d-flex p-2 ${hoveredIndex === i ? 'overlay' : ''}`}
            onMouseOver={() => handleMouseOver(i)}
            onMouseOut={handleMouseOut}
          >
            <Checkbox
              value={item.language_name}
              checked={selectedLanguages.includes(item.language_name)}
              onChange={handleCheckboxChange}
            />
            <LanguageName
              name={item.language_name}
              onClick={handleLangClick}
            />
          </div>
        );
      })}
    </div>
  );
}


const createSelectedLanguagesAtom = () => {
  return (
    atom([])
  )
}

function SelectLanguage(props) {
  const selectedLanguagesAtom = React.useMemo(createSelectedLanguagesAtom, []);
  const [isHover, setIshover] = useState(false);
  const [selectedLanguages, setSelectedLanguages] = useAtom(selectedLanguagesAtom);



  const [langInputClicked, setLangInputClicked] = useState(false);
  const [hoveredIndex, setHoveredIndex] = useState(null);
  const dropdownRef = useRef(null);

  console.log(langInputClicked);

  function selectMouseOver() {
    setIshover(true);
  }

  function selectMouseLeave() {
    setIshover(false);
  }

  const handleMouseOver = (index) => {
    setHoveredIndex(index);
  };

  const handleMouseOut = () => {
    setHoveredIndex(null);
  };

  useEffect(() => {
    if (props.country !== "United States") {
      setSelectedLanguages([]);
      // Perform any necessary actions for the updated props
    } else {
      setSelectedLanguages(['Spanish', 'English'])
    }
  }, [props.country]);

  useEffect(() => {
    function handleClickOutside(event) {

      console.log(event.target);
      if (dropdownRef.current &&   !dropdownRef.current.contains(event.target)&& !event.target.closest('.dropdownInput')) {
        console.log(dropdownRef.current);
        console.log("contains :" +event.target.closest('.dropdownInput') );
        setLangInputClicked(false);


      }



    }

    document.addEventListener('click', handleClickOutside);
    return () => {
      document.removeEventListener('click', handleClickOutside);
    };
  }, []);



  const handleCheckboxChange = (event) => {
    const { value, checked } = event.target;
    if (checked) {
      setSelectedLanguages((prevSelected) => [...prevSelected, value]);
    } else {
      setSelectedLanguages((prevSelected) =>
        prevSelected.filter((lang) => lang !== value)
      );
    }
  };



  const handleClick = () => {
    if (langInputClicked===false) {
      setLangInputClicked(true);

    } else {
      setLangInputClicked(false);
    }
  }
  function handleSelectCheck(event) {
    const { value, checked } = event.target;
    if (checked) {
      const allLanguages = props.language.map((item) => {
        return (
          item.language_name
        )
      })
      setSelectedLanguages(allLanguages);
    } else {
      setSelectedLanguages([]);
    }
  }
  function handleLangClick(event) {
    const languageName = event.target.innerHTML;
    if (selectedLanguages.includes(languageName)) {
      setSelectedLanguages((prevSelected) =>
        prevSelected.filter((item) => item !== languageName)
      );
    } else {
      setSelectedLanguages((prevSelected) => [...prevSelected, languageName]);
    }
  }
  function handleSelectClick() {
    if (props.language.length !== selectedLanguages.length) {
      const allLanguages = props.language.map((item) => {
        return (
          item.language_name
        )
      })
      setSelectedLanguages(allLanguages);
    } else {
      setSelectedLanguages([]);
    }

  }

  return (
    < >
      <div ref={dropdownRef}  className="w-lg-100 position-relative " >
        <input onClick={handleClick}
          style={{ cursor: 'pointer' }}
          className="form-control form-control-solid"
          type="text"
          placeholder='Select languages'
          defaultValue={selectedLanguages.join(', ')} // Display selected languages in the input

          onFocus={(e) => e.target.blur()}
        />
        <div className="position-absolute translate-middle-y top-50 end-0 me-3">
          <span className="svg-icon svg-icon-2hx">
            <i style={{ cursor: 'pointer' }} className="fa-solid fa-caret-down"></i>
          </span>
        </div>


        <div className="col-lg-12 dropdownInput" style={{ position: 'relative' }}>

          {langInputClicked && <Dropdown
            language={props.language}
            selectedLanguages={selectedLanguages}
            handleCheckboxChange={handleCheckboxChange}
            handleSelectCheck={handleSelectCheck}
            handleLangClick={handleLangClick}
            handleSelectClick={handleSelectClick}
            hoveredIndex={hoveredIndex}
            handleMouseOver={handleMouseOver}
            handleMouseOut={handleMouseOut}
            isHover={isHover}
            selectMouseOver={selectMouseOver}
            selectMouseLeave={selectMouseLeave}
          />
          }
        </div>

      </div>





    </>
  );
}

export default SelectLanguage;
