import {
    QueryClient,
    QueryClientProvider,
    useQuery,
} from '@tanstack/react-query'
import { Container, Row, Col } from 'react-bootstrap'
import { v4 as uuidv4 } from 'uuid';
import CountryOption from './CountryOption';
import { atom, useAtom } from 'jotai';
import SelectLanguage from './SelectLanguage';
import React, { useEffect, useRef, useState } from 'react';
import { isAppSelectedAtom } from './SearchApp';

const queryClient = new QueryClient()

const createCountryAtom = () => {    // function that create atom for every using this components multiple times

    return (
        atom({
            imageUrl: 'https://flagcdn.com/32x24/us.webp',
            country_name: 'United States',
            language: [{
                language_code: "es",
                language_name: "Spanish"
            }, {
                language_code: "en",
                language_name: "English"
            }]
        })
    )
};



export default function Selectcountry() {

    return (
        <QueryClientProvider client={queryClient}>
            <Selecting />
        </QueryClientProvider>
    )
}

function DropdownContent({ data, callBack}) {
    return (
        <div className='dropdown' style={{ position: 'relative',cursor:'pointer' }}>
            <div className='card shadow-sm' style={{ position: 'absolute', zIndex: 2, height: '220px', overflowY: 'auto' }}>
                {data.mapping.map((item) => {
                    const id = uuidv4();
                    if (item.country_code == 'us' || item.country_code == 'gb' || item.country_code == 'in') {
                        const imageUrls = `https://flagcdn.com/32x24/${item.country_code}.webp`;

                        return <CountryOption key={id} imageUrl={imageUrls} country_name={item.country_name} language={item.languages_list} selectedCountry={callBack} />;
                    }
                })}
                {data.mapping.map((item) => {
                    const id = uuidv4();
                    if (item.country_code === 'uk' || item.country_code === 'an') {
                        return null;
                    }
                    const imageUrls = `https://flagcdn.com/32x24/${item.country_code}.webp`;

                    return <CountryOption key={id} clicked={callBack} selectedCountry={callBack} imageUrl={imageUrls} language={item.languages_list} country_name={item.country_name} />;
                })}
            </div>
        </div>
    );
}

export function Selecting() {
    const [isAppSelected] = useAtom(isAppSelectedAtom);
    const countryAtom = React.useMemo(createCountryAtom, []);
    const dropdownRef = useRef(null);
    const [country, setCountry] = useAtom(countryAtom);
    const [isInputClicked, setInputClicked] = useState(false);
 
    const { isLoading, error, data } = useQuery({
        queryKey: ['repoData'],
        queryFn: () =>
            fetch('https://jsonserver.maakeetoo.com/get-mapping')
                .then((res) => res.json()),
        enabled: isAppSelected === true,
        staleTime: 300000,
        client: queryClient,
    });

    useEffect(() => {
        function handleClickOutside(event) {
        
      
            if (dropdownRef.current && !dropdownRef.current.contains(event.target)) {
                setInputClicked(false);
                
            }
        }

        document.addEventListener('click', handleClickOutside);
        return () => {
            document.removeEventListener('click', handleClickOutside);
        };
    }, []);

    function callBack(country) {
        setInputClicked(false); //automatically closes country dropdown once country selected
        setCountry(country);
    }

    function handleClick() {
        if (isInputClicked === false) {
            setInputClicked(true);
        } else {
            setInputClicked(false);
        }
    }

    return (
        <>
            <Container>
                <Row>
                    <Col lg="2">
                        <div ref={dropdownRef} className="w-lg-100 position-relative" onClick={handleClick}>
                            <input style={{ cursor: 'pointer' }} readOnly onFocus={(e) => e.target.blur()} className="form-control form-control-solid" type="text" />
                            <div className="position-absolute translate-middle-y top-50 end-0 me-3 mx-2">
                                <span className="svg-icon svg-icon-2hx">
                                    <i className="fa-solid fa-caret-down"></i>
                                </span>
                            </div>
                            <div style={{ cursor: 'pointer' }} className="position-absolute translate-middle-y mx-1 top-50 me-3">
                                <span className="svg-icon svg-icon-2hx">
                                    <img src={country.imageUrl} alt={country.country_name} />
                                </span>
                            </div>
                            {isInputClicked  && <DropdownContent data={data} callBack={callBack} />}

                        </div>
                    </Col>
                    <Col lg="10">
                        <SelectLanguage country={country.country_name} language={country.language} />
                    </Col>
                </Row>
            </Container>
        </>
    );
}
