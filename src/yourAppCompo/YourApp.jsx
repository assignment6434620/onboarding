import React, { useState } from "react";
import {  Col, Container, Row } from "react-bootstrap";
import Platform from "./Platform";
import Search from "./Search";
import Selectcountry from "./Selectcountry";
import { v4 as uuidv4 } from 'uuid';
import { isAppSelectedAtom } from "./SearchApp";
import { atom, useAtom } from "jotai";
import { toast, ToastContainer } from 'react-toastify';
import "react-toastify/dist/ReactToastify.css";
;

const isAddAppsCompleteAtom=atom(false);

function YourApp() {
    const [selectedCountries, setSelectedCountries] = useState([]);
    const [isAppSelected] = useAtom(isAppSelectedAtom);
    const [isAddAppsComplete,setAddAppsComplete]=useAtom(isAddAppsCompleteAtom);

    const addSelectCountry = () => {
        setSelectedCountries((prevSelectedCountries) => [
            ...prevSelectedCountries,
            { id: uuidv4(), country: null }
        ]);
    };

    function handleNextClick(){
        if(isAppSelected===false){
            toast.warn("Please select an app before submit", {
                position: "top-right",
                autoClose: 2000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                theme: "light",
                });
        }else{
            setAddAppsComplete(true);
        }
       
    }


    const handleDelete = (countryId) => {
        setSelectedCountries((prevSelectedCountries) => {
            const updatedCountries = prevSelectedCountries.filter(
                (country) => country.id !== countryId
            );
            return updatedCountries;
        });
    };

    return (
        <section  style={{ height: 'auto',display:isAddAppsComplete?'none':'block',borderRadius:"12.5px" }} id="your-app">
            <Container>
                <Row>
                    <Col lg="4">
                        <Platform />
                    </Col>
                    <Col lg="3">
                        <Search />
                    </Col>
                    <Col lg="5" style={{ display: isAppSelected ? "block" : "none" }} >
                        <Row>
                            <h3>Select countries and languages</h3>
                            <Col lg="12" className="mt-2">
                                <Selectcountry />
                                {selectedCountries.map((country) => (
                                    <div className="d-flex my-3" key={country.id}>
                                        <Selectcountry
                                            value={country.country}
                                        />
                                        <i
                                            onClick={() => handleDelete(country.id)}
                                            className="fa-solid fa-xmark fa-lg mt-3" style={{ color: 'red', cursor: 'pointer' }}
                                        ></i>
                                    </div>
                                ))}
                            </Col>
                            <Col lg="12">
                                <div className="mt-2 position-relative">
                                    <button
                                        onClick={addSelectCountry}
                                        style={{ marginLeft: '80%' }}
                                        className="position-relative btn btn-primary btn-sm btn-rounded"
                                    >
                                        Add More
                                    </button>
                                </div>
                            </Col>
                        </Row>
                    </Col>
                </Row>
                
                <Row  style={{position:"relative",paddingTop:"12.5%"}} >
                    <Col lg="12" className="text-center">
                        <button style={{boxSizing:"border-box",minWidth:"40px",padding:".5rem 2rem"}}  onClick={handleNextClick} href="#" className="btn btn-primary btn-lg h-38">Next</button>

                        <ToastContainer />
                    </Col>
                </Row>

            </Container>
       
        </section>
    );
}

export default YourApp;
export {isAddAppsCompleteAtom};
